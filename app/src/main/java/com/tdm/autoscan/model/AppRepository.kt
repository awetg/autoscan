package com.tdm.autoscan.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.tdm.autoscan.R
import com.tdm.autoscan.adapter.HomeItem
import android.content.Context
import android.util.Log
import android.widget.Toast
import com.tdm.autoscan.model.room.Car
import com.tdm.autoscan.model.room.MyDatabase
import com.tdm.autoscan.network.API
import com.tdm.autoscan.network.VINResponseModel
import com.tdm.autoscan.view.BLEConnectFragment
import com.tdm.autoscan.view.CarProfileFragment
import com.tdm.autoscan.view.LiveDataFragment
import com.tdm.autoscan.view.TroubleCodeFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback

object AppRepository {

    private var db: MyDatabase? = null


    private val _homeGridItems = MutableLiveData<MutableList<HomeItem>>().apply {
        if (value == null) {

            value =  mutableListOf(
                HomeItem("Not Connected","Connect your OBD 2 device", R.drawable.ic_bluetooth_red_64dp, BLEConnectFragment()),
                HomeItem("Live Data","Read live sensor data", R.drawable.ic_track_changes_black_64dp, LiveDataFragment()),
               // HomeItem("Monitor", R.drawable.ic_watch_later_black_64dp, CarProfileFragment.newInstance()),
                HomeItem("Read codes","Read DTC/Fault codes", R.drawable.ic_error_outline_pinkish_64dp, TroubleCodeFragment())
            )
        } else {
            value!!.addAll(mutableListOf(
                HomeItem("Live Data", "Read live sensor data", R.drawable.ic_track_changes_black_64dp,  CarProfileFragment()))
            )
        }
    }

    val homeGridItems: LiveData<MutableList<HomeItem>> = _homeGridItems

    private val _carProfile = MutableLiveData<Car?>().apply { value = null }

    val carProfile: LiveData<Car?> = _carProfile

    fun setBluetoothConnection(connected: Boolean) {
        val gridItems = homeGridItems.value
        if (gridItems!= null) {

            if (connected){
                gridItems[0] = HomeItem("Connected","Connected and configured", R.drawable.ic_bluetooth_connected_black_64dp, BLEConnectFragment())
                _homeGridItems.value = gridItems
            }
            else {
                gridItems[0] = HomeItem("Not Connected","Connect your OBD 2 device", R.drawable.ic_bluetooth_red_64dp, BLEConnectFragment())
                _homeGridItems.value = gridItems
            }
        } else
            _homeGridItems.value = mutableListOf(HomeItem("Connected", "",R.drawable.ic_bluetooth_connected_black_64dp, BLEConnectFragment()))
    }

    fun getCarProfile(vin: String, context: Context) {
        if (_carProfile.value != null)
            return
        else if (db == null)
            db = MyDatabase.getInstance(context)
        GlobalScope.launch(Dispatchers.IO) {
            var car = db?.carDao()?.selectCarWithVin(vin)
            if (car != null)
                GlobalScope.launch(Dispatchers.Main) {
                    _carProfile.value = car
                }
            else {

                val call = API.vinService.upload(vin)

                val value = object : Callback<VINResponseModel.Response> {
                    override fun onFailure(call: Call<VINResponseModel.Response>, t: Throwable) {
                        Log.e("DBG", "vin decode response failure:---${t.message.toString()}")
                        Toast.makeText(context, "Car information not found, check that you have correct vin number.", Toast.LENGTH_LONG)
                            .show()
                    }

                    override fun onResponse(
                        call: Call<VINResponseModel.Response>,
                        response: retrofit2.Response<VINResponseModel.Response>
                    ) {
                        val res = response.body()
                        if (res != null) {
                            val c = res.response.decodedVINValues

                            val car = Car(
                                c.vin,
                                c.bodyClass,
                                c.engineCylinders,
                                c.destinationMarket,
                                c.displacementCC,
                                c.displacementCI,
                                c.displacementL,
                                c.doors,
                                c.driveType,
                                c.engineModel,
                                c.engineKW,
                                c.fuelTypePrimary,
                                c.make,
                                c.manufacturer,
                                c.model,
                                c.modelYear,
                                c.plantCity,
                                c.seats,
                                c.series,
                                c.steeringLocation,
                                c.transmissionStyle,
                                c.vehicleType,
                                c.brakeSystemType,
                                c.engineHP,
                                c.plantState,
                                c.engineManufacturer,
                                c.manufacturerId,
                                c.TPMS
                            )
                            _carProfile.value = car
                            GlobalScope.launch(Dispatchers.IO) {
                                db?.carDao()?.insert(car)
                            }

                        }
                    }


                }
                call.enqueue(value)
            }
        }

    }

}