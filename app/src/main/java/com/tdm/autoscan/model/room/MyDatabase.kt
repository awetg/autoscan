package com.tdm.autoscan.model.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Car::class], version = 1, exportSchema = false)

abstract class MyDatabase: RoomDatabase(){

    abstract fun carDao(): CarDao

    companion object {

        @Volatile
        private var INSTANCE: MyDatabase? = null

        @Synchronized
        fun getInstance(context: Context): MyDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context.applicationContext, MyDatabase::class.java,"App.db")
                    .build()
            }
            return INSTANCE!!
        }
    }
}