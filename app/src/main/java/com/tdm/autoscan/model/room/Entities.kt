package com.tdm.autoscan.model.room

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "car_info")
data class Car(
    @PrimaryKey
    val vin: String,
    val bodyClass: String? = null,
    val engineCylinders: Int? = null,
    val destinationMarket: String? = null,
    val displacementCC: Double? = null,
    val displacementCI: Double? = null,
    val displacementL: Double? = null,
    val doors: Int? = null,
    val driveType: String? = null,
    val engineModel: String? = null,
    val engineKW: Double? = null,
    val fuelTypePrimary: String? = null,
    val make: String? = null,
    val manufacturer: String? = null,
    val model: String? = null,
    val modelYear: Int? = null,
    val plantCity: String? = null,
    val seats: Int? = null,
    val series: String? = null,
    val steeringLocation: String? = null,
    val transmissionStyle: String? = null,
    val vehicleType: String? = null,
    val brakeSystemType: String? = null,
    val engineHP: Double? = null,
    val plantState: String? = null,
    val engineManufacturer: String? = null,
    val manufacturerId: Double? = null,
    val TPMS: String? = null

)