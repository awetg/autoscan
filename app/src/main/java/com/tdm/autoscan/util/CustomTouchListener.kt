package com.tdm.autoscan.util

import android.view.MotionEvent
import android.view.TouchDelegate
import android.view.View

class CustomTouchListener : View.OnTouchListener{
    private var dX: Float = 0.toFloat()
    private var dY: Float = 0.toFloat()


    override fun onTouch(view: View, event: MotionEvent): Boolean {

        val newX: Float
        val newY: Float

        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                dX = view.x - event.rawX
                dY = view.y - event.rawY
            }
            MotionEvent.ACTION_MOVE -> {

                newX = event.rawX + dX
                newY = event.rawY + dY

                view.animate()
                    .x(newX)
                    .y(newY)
                    .setDuration(0)
                    .start()
            }
            else -> return false

        }
        return true
    }



}