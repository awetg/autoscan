package com.tdm.autoscan.util

import android.bluetooth.BluetoothManager
import android.bluetooth.BluetoothSocket
import android.content.Context
import android.util.Log
import com.github.pires.obd.commands.protocol.*
import com.github.pires.obd.enums.ObdProtocols
import com.github.pires.obd.exceptions.MisunderstoodCommandException
import com.github.pires.obd.exceptions.NoDataException
import com.github.pires.obd.exceptions.UnableToConnectException
import com.tdm.autoscan.model.AppRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException
import java.util.*

object BluetoothConnection {

    var socket: BluetoothSocket? = null

    var deviceAddress: String? = null

    private val MY_UUID: UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")

    // suspend method to make sure it is called inside coroutine or another suspend function
    suspend fun connect(context: Context): Boolean {

        return withContext(Dispatchers.IO) {

            if ((socket == null || !socket!!.isConnected) && deviceAddress != null) {
                Log.d("DBG", "Trying to connect...")
                val bluetoothManager = context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
                var tempSocket: BluetoothSocket? = null
                val socketFallback: BluetoothSocket
                try {
                    val device = bluetoothManager.adapter.getRemoteDevice(deviceAddress)
                    tempSocket = device.createRfcommSocketToServiceRecord(MY_UUID)
                    tempSocket!!.connect()
                    socket = tempSocket
                } catch (e: Exception) {
                    Log.d("DBG","There was an error while establishing Bluetooth connection. Falling back..")
                    e.printStackTrace()
                    if (tempSocket != null) {
                        val clazz = tempSocket.remoteDevice.javaClass
                        val paramTypes = arrayOf<Class<*>>(Integer.TYPE)
                        try {
                            val method = clazz.getMethod("createRfcommSocket", *paramTypes)
                            socketFallback = method.invoke(tempSocket.remoteDevice, Integer.valueOf(1)) as BluetoothSocket
                            socketFallback.connect()
                            socket = socketFallback
                        } catch (e: Exception) {
                            e.printStackTrace()
                            socket?.close()
                            Log.d("DBG","Couldn't fallback while establishing Bluetooth connection. error message: ${e.message}")
                        }

                    }
                }

            } else
                Log.d("DBG", "device address null.")

            (socket != null && socket!!.isConnected)
        }


    }

    suspend fun configureSocket(): BluetoothSocket? {
        return withContext(Dispatchers.IO) {

            val tempSocket = socket

            if (tempSocket != null) {

                Log.d("DBG", "Configuring OBD connection..")

                try {
                    Log.d("DBG", "Queueing jobs for connection configuration..")

                    ObdResetCommand().run(tempSocket.inputStream, tempSocket.outputStream)

                    // wait to 500ms for obd device reset
                    try {
                        Thread.sleep(500)                                                                                                                           
                    }catch (e: InterruptedException) {
                        Log.d("DBG", "thread sleep error ${e.message}")
                        e.printStackTrace()
                    }
                    EchoOffCommand().run(tempSocket.inputStream, tempSocket.outputStream)
                    EchoOffCommand().run(tempSocket.inputStream, tempSocket.outputStream)
                    LineFeedOffCommand().run(tempSocket.inputStream, tempSocket.outputStream)
                    TimeoutCommand(62).run(tempSocket.inputStream, tempSocket.outputStream)
                    SelectProtocolCommand(ObdProtocols.AUTO).run(tempSocket.inputStream, tempSocket.outputStream)

                    Log.d("DBG" , "Configuration complete")
                } catch (e: Exception) {
                    e.printStackTrace()
                    val code = when (e) {
                        is IOException -> {
                            Constant.OBD_COMMAND_FAILURE_IO
                            socket = null
                        }
                        is InterruptedException -> Constant.OBD_COMMAND_FAILURE_IE
                        is UnableToConnectException -> Constant.OBD_COMMAND_FAILURE_UTC
                        is MisunderstoodCommandException -> Constant.OBD_COMMAND_FAILURE_MIS
                        is NoDataException -> Constant.OBD_COMMAND_FAILURE_NODATA
                        else -> Constant.OBD_COMMAND_FAILURE
                    }
                    Log.d("DBG", "message: ${e.message} code: $code")
                }
            }

            socket
        }

    }

    suspend fun closeSocket() {
        GlobalScope.launch(Dispatchers.IO) {
            AppRepository.setBluetoothConnection(false)
            if (socket != null) {
                try {
                    socket?.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                    Log.d("DBG", "socket close error ${e.message}")
                }
            }
        }
    }
}