package com.tdm.autoscan.util

import com.github.pires.obd.commands.ObdCommand
import com.github.pires.obd.commands.SpeedCommand
import com.github.pires.obd.commands.engine.LoadCommand
import com.github.pires.obd.commands.engine.RPMCommand
import com.github.pires.obd.commands.fuel.FuelLevelCommand
import com.github.pires.obd.commands.pressure.FuelPressureCommand
import com.github.pires.obd.commands.temperature.AirIntakeTemperatureCommand
import com.github.pires.obd.commands.temperature.EngineCoolantTemperatureCommand
import de.nitri.gauge.Gauge

class CustomGauge(
    val maxValue: Float,
    val minValue: Float,
    val totalNicks: Int,
    val valuePerNick: Float,
    val majorNickInterval: Int,
    val moveToValue: Float,
    val upperText: String,
    val lowerText: String,
    val command: ObdCommand,
    var gauge: Gauge? = null
)

object CustomGaugeItems {
    val customGauges: HashMap<String, CustomGauge> = hashMapOf()

    init {
        customGauges.put(
            "Air Intake Temperature",
            CustomGauge(60f, -60f, 16, 10f, 2, 0f, "Intake", "C", AirIntakeTemperatureCommand())
        )
        customGauges.put(
            "Engine Coolant Temperature",
            CustomGauge(120f, -50f, 20, 10f, 2, 0f, "COOLANT", "C", EngineCoolantTemperatureCommand())
        )
        customGauges.put(
            "Engine Load",
            CustomGauge(100f, 0f, 14, 10f, 2, 0f, "LOAD", "%", LoadCommand())
        )
        customGauges.put(
            "Engine RPM",
            CustomGauge(8000f, 0f, 8, 1000f, 2, 0f, "RPM", "RPM", RPMCommand())
        )
        customGauges.put(
            "Vehicle Speed",
            CustomGauge(240f, 0f, 32, 10f, 2, 0f, "SPEED", "Km/h", SpeedCommand())
        )
        customGauges.put(
            "Fuel Pressure",
            CustomGauge(800f, 0f, 18, 50f, 2, 0f, "FUEL PR", "kPa", FuelPressureCommand())
        )
        customGauges.put(
            "Fuel Level",
            CustomGauge(100f, 0f, 14, 10f, 2, 0f, "FUEL Level", "%", FuelLevelCommand())
        )
    }
}