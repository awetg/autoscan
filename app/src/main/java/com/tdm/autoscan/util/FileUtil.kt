package com.tdm.autoscan.util

import android.content.Context
import android.os.Environment
import android.util.Log
import java.io.*
import java.util.zip.ZipInputStream

object FileUtil {

    fun copyStreamToFile(inputStream: InputStream, outputFile: File): File {
        inputStream.use { input ->
            val outputStream = FileOutputStream(outputFile)
            outputStream.use { output ->
                val buffer = ByteArray(4 * 1024) // buffer size
                while (true) {
                    val byteCount = input.read(buffer)
                    if (byteCount < 0) break
                    output.write(buffer, 0, byteCount)
                }
                output.flush()
            }
        }
        return outputFile
    }

    // create file on external App package DOCUMENT folder with given extension
    fun getOrCreateFile(context: Context, fileName: String, extension: String): File {
        val folder = context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)
        val file = File(folder, fileName + extension)
        if(!file.exists()) file.parentFile.mkdirs()
        return file
    }

    // create file on external App package DOCUMENT folder extension can passed with file name
    fun getOrCreateFile(context: Context, fileName: String): File {
        val folder = context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)
        val file = File(folder, fileName)
        if(!file.exists()) file.parentFile.mkdirs()
        return file
    }

    // create directory with given name inside App Document folder
    fun createDirIfNotExist(context: Context, dirName: String): Boolean {
        val folder = context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS + File.separator + dirName)
        if (folder != null) {
            return if (folder.exists()) true else folder.mkdir()
        } else {
            return false
        }
    }

    // unzip region road condition kml file
    fun unzipRegionKMLFile(context: Context, regionName: String): Boolean {
        val folder = context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)
        val file = File(folder, "$regionName.zip")
        try {
            val zipInputStream = ZipInputStream(FileInputStream(file))
            var zipEntry = zipInputStream.nextEntry
            val b = ByteArray(1024)
            while (zipEntry != null) {
                if (zipEntry.isDirectory) createDirIfNotExist(context, zipEntry.name)
                else {
                    val fileOutputStream = FileOutputStream(getOrCreateFile(context, zipEntry.name))
                    val bufferedInputStream = BufferedInputStream(zipInputStream)
                    val bufferedOutputStream = BufferedOutputStream(fileOutputStream)
                    var n = bufferedInputStream.read(b, 0, 1024)
                    while (n >= 0) {
                        bufferedOutputStream.write(b, 0, n)
                        n = bufferedInputStream.read(b, 0, 1024)
                    }
                    zipInputStream.closeEntry()
                    bufferedOutputStream.close()
                }
                zipEntry = zipInputStream.nextEntry
            }
            zipInputStream.close()
            Log.d("DBG", "unzip complete")
            return true

        } catch (e: Exception) {
            Log.d("DBG", e.message)
            e.printStackTrace()
            return false
        }
    }
}