package com.tdm.autoscan.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tdm.autoscan.R
import com.tdm.autoscan.network.API
import kotlinx.android.synthetic.main.error_code_row.view.*
import kotlinx.android.synthetic.main.on_expand_view.view.*
import java.util.*


class DTCAdapter : RecyclerView.Adapter<DTCAdapter.DTCViewHolder>() {

    private var troubleCodes: MutableList<String> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DTCViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.error_code_row, parent, false)
        return DTCViewHolder(view)

    }

    override fun getItemCount(): Int {
        return troubleCodes.size
    }

    override fun onBindViewHolder(holder: DTCViewHolder, position: Int) {
        holder.setData(troubleCodes[position])
    }

    fun addAllCodes(troubleCodes : MutableList<String>) {
        this.troubleCodes = troubleCodes
        notifyDataSetChanged()
    }

    fun addCode(code: String) {
        troubleCodes.add(code)
        notifyDataSetChanged()
    }

    inner class DTCViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val apiResponseCallback = object : API.APIResponseCallback<API.Model.DTCResponse> {
            override fun onSuccess(value: API.Model.DTCResponse) {
                Log.d("DBG", "OnSucessValue.code${value.code}")
           itemView.error_location_textView.text = value.location
           itemView.possible_cause_textView.text = value.probable_cause
            }

            override fun onFailure(throwable: Throwable) {
                Log.d("DBG", "${throwable.message}")
            }
        }

        fun setData(troubleCode: String) {
            itemView.card_title.text = troubleCode
            API.makeDTCRequest(troubleCode, apiResponseCallback)

            itemView.card_arrow.setOnClickListener{
                if(itemView.expandable_view_layout.visibility == View.GONE){
                itemView.expandable_view_layout.visibility = View.VISIBLE
                }else{
                    itemView.expandable_view_layout.visibility = View.GONE
                }
            }

        }

    }
}
