package com.tdm.autoscan.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.tdm.autoscan.R
import kotlinx.android.synthetic.main.region_row_layout.view.*

data class RegionSubscription(val name: String, var isSubscribed: Boolean)

class RegionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

class RegionsAdapter (val regionSubscriptions: List<RegionSubscription>) : RecyclerView.Adapter<RegionViewHolder>() {

    private var clickListener: (Boolean, RegionSubscription) -> Unit = { _, _ -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RegionViewHolder {
        return RegionViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.region_row_layout, parent, false) as LinearLayout
        )
    }

    override fun getItemCount(): Int  = regionSubscriptions.size

    fun setClickListener(newClickListner: (Boolean, RegionSubscription) -> Unit) {
        clickListener = newClickListner
    }

    override fun onBindViewHolder(holder: RegionViewHolder, position: Int) {
        val regionSubscription = regionSubscriptions[position]
        holder.itemView.region_switch.text = regionSubscription.name
        holder.itemView.region_switch.isChecked = regionSubscription.isSubscribed
        holder.itemView.region_switch.setOnCheckedChangeListener { _, isChecked ->  clickListener(isChecked, regionSubscription) }
    }
}