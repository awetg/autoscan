package com.tdm.autoscan.network


import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

object VINResponseModel{

    @Root(name = "Response", strict = false )
    data class Response(@field:Element(name = "Count", required = false) @param:Element(name = "Count", required = false)val count:Int,
                        @field:Element(name = "Message", required = false) @param:Element(name = "Message", required = false)val message: String,
                        @field:Element(name = "Results") @param:Element(name = "Results")val response: Results)

    @Root(name = "Results", strict = false )
    data class Results( @field:Element(name = "DecodedVINValues") @param:Element(name = "DecodedVINValues")val decodedVINValues: DecodedVINValues)

    @Root(name = "DecodedVINValues", strict = false )
    data class DecodedVINValues(
        @field:Element(name = "VIN", required = false) @param:Element(name = "VIN", required = false)val vin: String,
        @field:Element(name = "BodyClass", required = false) @param:Element(name = "BodyClass", required = false)val bodyClass: String? = null,
        @field:Element(name = "EngineCylinders", required = false) @param:Element(name = "EngineCylinders", required = false) val engineCylinders: Int? = null,
        @field:Element(name = "DestinationMarket", required = false) @param:Element(name = "DestinationMarket", required = false)val destinationMarket: String? = null,
        @field:Element(name = "DisplacementCC", required = false) @param:Element(name = "DisplacementCC", required = false)val displacementCC: Double? = null,
        @field:Element(name = "DisplacementCI", required = false) @param:Element(name = "DisplacementCI", required = false)val displacementCI: Double? = null,
        @field:Element(name = "DisplacementL", required = false) @param:Element(name = "DisplacementL", required = false) val displacementL: Double? = null,
        @field:Element(name = "Doors", required = false) @param:Element(name = "Doors", required = false)val doors: Int? = null,
        @field:Element(name = "DriveType", required = false) @param:Element(name = "DriveType", required = false)val driveType: String? = null,
        @field:Element(name = "EngineModel", required = false) @param:Element(name = "EngineModel", required = false)val engineModel: String? = null,
        @field:Element(name = "EngineKW", required = false) @param:Element(name = "EngineKW", required = false) val engineKW: Double? = null,
        @field:Element(name = "FuelTypePrimary", required = false) @param:Element(name = "FuelTypePrimary", required = false)val fuelTypePrimary: String? = null,
        @field:Element(name = "Make", required = false) @param:Element(name = "Make", required = false)val make: String? = null,
        @field:Element(name = "Manufacturer", required = false) @param:Element(name = "Manufacturer", required = false)val manufacturer: String? = null,
        @field:Element(name = "Model", required = false) @param:Element(name = "Model", required = false)val model: String? = null,
        @field:Element(name = "ModelYear", required = false) @param:Element(name = "ModelYear", required = false)val modelYear: Int? = null,
        @field:Element(name = "PlantCity", required = false) @param:Element(name = "PlantCity", required = false)val plantCity: String? = null,
        @field:Element(name = "Seats", required = false) @param:Element(name = "Seats", required = false)val seats: Int? = null,
        @field:Element(name = "Series", required = false) @param:Element(name = "Series", required = false)val series: String? = null,
        @field:Element(name = "SteeringLocation", required = false) @param:Element(name = "SteeringLocation", required = false)val steeringLocation: String? = null,
        @field:Element(name = "TransmissionStyle", required = false) @param:Element(name = "TransmissionStyle", required = false)val transmissionStyle: String? = null,
        @field:Element(name = "VehicleType", required = false) @param:Element(name = "VehicleType", required = false)val vehicleType: String? = null,
        @field:Element(name = "BrakeSystemType", required = false) @param:Element(name = "BrakeSystemType", required = false)val brakeSystemType: String? = null,
        @field:Element(name = "EngineHP", required = false) @param:Element(name = "EngineHP", required = false)val engineHP: Double? = null,
        @field:Element(name = "PlantState", required = false) @param:Element(name = "PlantState", required = false)val plantState: String? = null,
        @field:Element(name = "EngineManufacturer", required = false) @param:Element(name = "EngineManufacturer", required = false)val engineManufacturer: String? = null,
        @field:Element(name = "ManufacturerId", required = false) @param:Element(name = "ManufacturerId", required = false)val manufacturerId: Double? = null,
        @field:Element(name = "TPMS", required = false) @param:Element(name = "TPMS", required = false)val TPMS: String? = null
    )
}

