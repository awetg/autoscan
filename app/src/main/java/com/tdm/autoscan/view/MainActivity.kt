package com.tdm.autoscan.view

import android.app.NotificationChannel
import android.app.NotificationManager
import android.bluetooth.BluetoothManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatDelegate
import com.tdm.autoscan.R
import kotlinx.android.synthetic.main.activity_main.*
import com.tdm.autoscan.util.BluetoothConnection
import com.tdm.autoscan.util.Constant


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        // changing App theme is considered configuration change,
        // so we need to set this before activity is created so that android doesn't recreate MainActivity
         AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val startFragment = intent.getStringExtra(Constant.START_FRAGMENT)
        if (startFragment == "NotificationFragment")
            addFragment(NotificationFragment.newInstance(true))
        else if (startFragment == "LiveDataFragment")
            addFragment(HomeFragment.newInstance(true))
        else
            addFragment(HomeFragment())

        bottom_navigation_view.setOnNavigationItemSelectedListener { item ->
            when(item.itemId) {
                R.id.nav_home -> addFragment(HomeFragment())
                R.id.nav_profile -> addFragment(CarProfileFragment())
                R.id.notification -> addFragment(NotificationFragment())
            }
            return@setOnNavigationItemSelectedListener true
        }


//        RuntimePermissionUtil.getInstance(this).requestAllUnGrantedermissions()

//        val token = getSharedPreferences("USER_DATA", Context.MODE_PRIVATE).getString("TOKEN", "Not set")

        BluetoothConnection.deviceAddress = getSharedPreferences(Constant.BLUETOOTH_DATA, Context.MODE_PRIVATE)
            .getString(Constant.SELECTED_DEVICE_ADDRESS, null)

        val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        if (!bluetoothManager.adapter.isEnabled)
            bluetoothManager.adapter.enable()


        createNotificationChannel(
            "CHANNEL_ID",
            "CHANNEL_NAME",
            "CHANNEL_DESCRIPTION",
            NotificationManager.IMPORTANCE_HIGH
        )

    }



    // Creates notification channel with given params
    private fun createNotificationChannel(channelId: String, channelName: String, channelDescription: String, priority: Int) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId, channelName,priority)
            channel.description = channelDescription
            val notificationManager: NotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun addFragment(fragment: Fragment) {
        supportFragmentManager.popBackStack()
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragment_container, fragment)
            commit()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.settings_menu) {
            startActivity(Intent(this, SettingsActivity::class.java))
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}
