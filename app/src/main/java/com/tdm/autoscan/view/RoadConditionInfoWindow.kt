package com.tdm.autoscan.view

import kotlinx.android.synthetic.main.road_condition_infowindow_layout.view.*
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.OverlayWithIW
import org.osmdroid.views.overlay.infowindow.InfoWindow

class RoadConditionInfoWindow(layoutResourceId: Int, mapView: MapView, val extendedData: HashMap<String, String>): InfoWindow(layoutResourceId, mapView) {

    init {
//        mapView.setOnTouchListener { view, motionEvent ->
//            if (motionEvent.action == MotionEvent.ACTION_UP)
//                close()
//            return@setOnTouchListener true
//        }
    }

    override fun onOpen(item: Any?) {
        val overlay = item as OverlayWithIW
        mView.overall_val_txt.text = if (extendedData["overallRoadCondition"] == "POOR_CONDITION") "Poor" else "Extremely poor"
        mView.time_val_txt.text = extendedData["time"]
        mView.roadTem_val_txt.text = extendedData["roadTemperature"]
        mView.temp_val_txt.text = extendedData["temperature"]
        mView.windSpeed_val_txt.text = extendedData["windSpeed"]
        mView.windDir_val_txt.text = extendedData["windDirection"]

    }

    override fun onClose() {}

}