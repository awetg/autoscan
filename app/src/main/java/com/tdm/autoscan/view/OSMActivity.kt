package com.tdm.autoscan.view

import android.content.Context
import android.icu.text.SimpleDateFormat
import android.icu.util.TimeZone
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.tdm.autoscan.util.Constant
import kotlinx.android.synthetic.main.activity_osm.*
import org.osmdroid.bonuspack.kml.*
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.views.overlay.*
import com.tdm.autoscan.util.FileUtil
import org.osmdroid.util.BoundingBox
import java.lang.Exception
import java.lang.ref.WeakReference
import java.util.*
import kotlin.collections.HashMap


class OSMActivity : AppCompatActivity() {

    private val recycledRegionOverlays: HashMap<String, FolderOverlay> = hashMapOf()
    private var subscribedRegions: Set<String> = setOf()
    private val boundingBoxes: MutableList<BoundingBox> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        Configuration.getInstance().load(this, androidx.preference.PreferenceManager.getDefaultSharedPreferences(this))
        super.onCreate(savedInstanceState)
        setContentView(com.tdm.autoscan.R.layout.activity_osm)

        val sharedPref = getSharedPreferences(Constant.APP_DATA, Context.MODE_PRIVATE)
        val updateTime = sharedPref.getString(Constant.ROAD_CONDITION_UPDATE_TIME, null)    // last time road alert data updated
        subscribedRegions = sharedPref.getStringSet(Constant.SUBSCRIBED_REGIONS_KEY, null) ?: setOf()   // set of region names subscribed for alerts

        val dateText: String
        val time0h: String
        val time12h: String

        if (updateTime != null) {
            val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US)
            parser.timeZone = TimeZone.getTimeZone("US")
            val date = parser.parse(updateTime)
            dateText = SimpleDateFormat("yyyy-MM-dd").format(date)
            time0h = SimpleDateFormat("HH:mm").format(date)
            time12h = SimpleDateFormat("HH:mm").format(addHoursToDate(date, 12))

        } else {
            val date = Calendar.getInstance().time
            dateText = SimpleDateFormat("yyyy-MM-dd").format(date)
            time0h = SimpleDateFormat("HH:mm").format(date)
            time12h = SimpleDateFormat("HH:mm").format(addHoursToDate(date, 12))
        }
        date_txt.text = dateText
        zeroh_btn.text = time0h
        twelveh_btn.text = time12h

        // show road alert(road condition) overlay for 0h or observation for each subscribed region
        zeroh_btn.setOnClickListener {
            // if overlay is built before add the from recycling store else load from file
            val overlays = subscribedRegions.map { recycledRegionOverlays["$it-0h.kml"] }.filterNotNull()
            if (overlays.isNotEmpty()) {
                map.overlays.addAll(overlays)
                map.invalidate()
            } else {
                subscribedRegions.forEach { RegionKMLLoader(this).execute("$it-0h.kml") }
            }
        }
        // show road alert(road condition) overlay for 12h forecast for each subscribed region
        twelveh_btn.setOnClickListener {
            val overlays = subscribedRegions.map { recycledRegionOverlays["$it-12h.kml"] }.filterNotNull()
            if (overlays.isNotEmpty()) {
                map.overlays.removeAll { it is FolderOverlay }
                map.overlays.addAll(overlays)
                map.invalidate()
            } else {
                subscribedRegions.forEach { RegionKMLLoader(this).execute("$it-12h.kml") }
            }
        }


        map.setTileSource(TileSourceFactory.MAPNIK)
        map.setMultiTouchControls(true)

        Handler(Looper.getMainLooper()).postDelayed({
            // Finland's bounding box
            val bbox = BoundingBox(70.0922939,31.5867044,59.4541578,19.0832098)
            map.zoomToBoundingBox(bbox, false)
            map.controller.setCenter(bbox.center)
            map.invalidate()
            subscribedRegions.forEach { RegionKMLLoader(this).execute("$it-0h.kml") }
        }, 10)

    }

    private fun addHoursToDate(date: Date, hours: Int): Date {
        val calendar = Calendar.getInstance()
        calendar.time = date
        calendar.add(Calendar.HOUR_OF_DAY, hours)
        return calendar.time
    }


    private class RegionKMLLoader internal constructor(context: OSMActivity) : AsyncTask<String, Unit, FolderOverlay>() {

        private val activityReference: WeakReference<OSMActivity> = WeakReference(context)

        private lateinit var kmlDocument: KmlDocument

        override fun doInBackground(vararg fileNameParam: String): FolderOverlay {

            val activity = activityReference.get()

            if (activity != null) {

                val map = activity.map

                kmlDocument = KmlDocument()
                val file = FileUtil.getOrCreateFile(activity.applicationContext, fileNameParam[0])
                kmlDocument.parseKMLFile(file)
                val kmlOverlay = kmlDocument.mKmlRoot.buildOverlay(map, null, KMLFeatureStyler, kmlDocument) as FolderOverlay
                kmlOverlay.name = fileNameParam[0]

                return kmlOverlay
            } else {
                return FolderOverlay()
            }

        }

        override fun onPostExecute(kmlOverlay: FolderOverlay?) {
            val activity = activityReference.get()

            if (kmlOverlay != null && activity != null) {
                val map = activity.map
                map.overlays.add(kmlOverlay)
                // add to recycle list for recycling later
                activity.recycledRegionOverlays.put(kmlOverlay.name, kmlOverlay)
                if (kmlDocument.mKmlRoot.boundingBox != null)
                    activity.boundingBoxes.add(kmlDocument.mKmlRoot.boundingBox)
                activity.zoomToBoundingBoxes()
                map.invalidate()
            }
        }
    }

    fun zoomToBoundingBoxes() {
        if (boundingBoxes.isNotEmpty()) {
            try {
                val boundingBox = if (boundingBoxes.size > 1)
                    boundingBoxes.reduce { acc, boundingBox ->  acc.concat(boundingBox) }
                else
                    boundingBoxes[0]
                map.zoomToBoundingBox(boundingBox, false)
            } catch (e: Exception) {
                Log.d("DBG", e.message)
            }
        }
    }
}