package com.tdm.autoscan.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.tdm.autoscan.R
import com.tdm.autoscan.adapter.HomeRecyclerAdapter
import com.tdm.autoscan.model.AppRepository
import com.tdm.autoscan.util.BluetoothConnection
import com.tdm.autoscan.viewModel.HomeViewModel
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.coroutines.launch

private const val OPEN_LIVE_DATA = "param1"

class HomeFragment : BaseFragment() {

    private lateinit var homeViewModel: HomeViewModel
    private lateinit var homeRecyclerAdapter: HomeRecyclerAdapter
    private var openLiveData = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            openLiveData = it.getBoolean(OPEN_LIVE_DATA)
        }

        if (openLiveData)
            addFragment(LiveDataFragment())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)

        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        homeRecyclerAdapter = HomeRecyclerAdapter(arrayListOf())

        homeViewModel.items.observe(viewLifecycleOwner, Observer { Log.d("DBG", "items observer"); homeRecyclerAdapter.purgedAdd(it) })

        view.grid_recyclerview.layoutManager = LinearLayoutManager(activity)
        view.grid_recyclerview.adapter = homeRecyclerAdapter
        homeRecyclerAdapter.setClickListener { addFragment(it.fragment) }

        return view
    }

    private fun addFragment(fragment: Fragment) {
        activity?.supportFragmentManager?.beginTransaction()?.apply {
            replace(R.id.fragment_container, fragment)
            addToBackStack(null)
            commit()
        }
    }


    override fun onResume() {
        super.onResume()
        if (BluetoothConnection.socket != null && BluetoothConnection.socket!!.isConnected) {
            AppRepository.setBluetoothConnection(true)
            launch { BluetoothConnection.configureSocket() }

        }
        else
            launch {
                if (BluetoothConnection.connect(activity!!) ) {
                    AppRepository.setBluetoothConnection(true)
                    BluetoothConnection.configureSocket()
                }
            }
    }

    companion object {
        @JvmStatic
        fun newInstance(openMap: Boolean) =
            HomeFragment().apply {
                arguments = Bundle().apply {
                    putBoolean(OPEN_LIVE_DATA, openMap)
                }
            }
    }
}
