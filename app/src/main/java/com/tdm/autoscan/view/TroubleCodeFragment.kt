package com.tdm.autoscan.view

import android.bluetooth.BluetoothSocket
import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.pires.obd.commands.control.TroubleCodesCommand
import com.github.pires.obd.exceptions.MisunderstoodCommandException
import com.github.pires.obd.exceptions.NoDataException
import com.github.pires.obd.exceptions.UnableToConnectException
import com.tdm.autoscan.R
import com.tdm.autoscan.adapter.DTCAdapter
import com.tdm.autoscan.util.BluetoothConnection
import com.tdm.autoscan.util.Constant
import kotlinx.android.synthetic.main.fragment_trouble_code.*
import kotlinx.android.synthetic.main.fragment_trouble_code.view.*
import kotlinx.coroutines.launch
import java.io.IOException
import java.lang.ref.WeakReference

class TroubleCodeFragment : BaseFragment() {

    lateinit var dtcAdapter: DTCAdapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_trouble_code, container, false)

        dtcAdapter = DTCAdapter()
        view.trouble_codes_recycler_view.adapter = dtcAdapter
        view.trouble_codes_recycler_view.layoutManager = LinearLayoutManager(activity)
//        dtcAdapter.addAllCodes(mutableListOf("P0107", "P0106", "P0105", "P0104", "P0103"))

        launch {
            if (BluetoothConnection.connect(activity!!)) {
                val socket = BluetoothConnection.configureSocket()
                if (socket != null)
                    GetTroubleCode(socket, this@TroubleCodeFragment).execute()
            }
        }

        return view
    }

    private class GetTroubleCode internal constructor(private val socket: BluetoothSocket, context: TroubleCodeFragment) : AsyncTask<Unit, Unit, String>() {

        private val fragmentReference: WeakReference<TroubleCodeFragment> = WeakReference(context)


        override fun doInBackground(vararg p0: Unit?): String {

            if (fragmentReference.get() != null) {
                synchronized(this) {
                    try {
                        val troubleCodesCommand = TroubleCodesCommand()
                        troubleCodesCommand.run(socket.inputStream, socket.outputStream)
                        return troubleCodesCommand.formattedResult
                    } catch (e: Throwable) {
                        e.printStackTrace()
                        val code = when (e) {
                            is IOException -> Constant.OBD_COMMAND_FAILURE_IO
                            is InterruptedException -> Constant.OBD_COMMAND_FAILURE_IE
                            is UnableToConnectException -> Constant.OBD_COMMAND_FAILURE_UTC
                            is MisunderstoodCommandException -> Constant.OBD_COMMAND_FAILURE_MIS
                            is NoDataException -> Constant.OBD_COMMAND_FAILURE_NODATA
                            else -> Constant.OBD_COMMAND_FAILURE
                        }
                        Log.d("DBG", "message: ${e.message} code: $code")
                        return "Error occurred"
                    }
                }
            } else
                return ""
        }

        override fun onPostExecute(result: String?) {
            val fragment = fragmentReference.get()
            if (fragment != null && result != null && result.isNotEmpty()) {
                val codeList = result.split("\n").filter { it.isNotEmpty() }.toMutableList()
                fragment.dtcAdapter.addAllCodes(codeList)
            }
        }
    }

}


