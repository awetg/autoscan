package com.tdm.autoscan.view

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.github.pires.obd.commands.control.VinCommand
import com.tdm.autoscan.R
import com.tdm.autoscan.util.BluetoothConnection
import com.tdm.autoscan.viewModel.CarViewModel
import com.tdm.autoscan.util.Constant
import kotlinx.android.synthetic.main.fragment_car_profile.*
import kotlinx.coroutines.launch
import org.apache.commons.lang3.StringUtils


class CarProfileFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_car_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var vinNumber = activity?.getSharedPreferences(Constant.PROFILE_PREFERENCE_FILE_KEY,Context.MODE_PRIVATE)
            ?.getString(Constant.PREFERENCE_VIN, "3C4PDCBG1CT181079")

        val viewModel = ViewModelProviders.of(this).get(CarViewModel::class.java)

        if (viewModel.carProfile.value == null) {

            // read vin number if there connected OBD 2 device
            if (BluetoothConnection.socket != null && BluetoothConnection.socket!!.isConnected)
                launch {
                    try {

                        val socket = BluetoothConnection.socket
                        if (socket != null) {
                            val vinCommand = VinCommand()
                            vinCommand.run(socket.inputStream, socket.outputStream)
                            viewModel.getCarWithVin(vinCommand.formattedResult)
                        }
                    } catch (e: Exception) {
                        Log.d("DBG", "vin reading exception ${e.message}")

                        // if reading vin number is failed show last connected car
                        if (vinNumber != null)
                            viewModel.getCarWithVin(vinNumber)
                    }
                }
            // else show profile of last connected car or last entered vin number
            else if (vinNumber != null)
                viewModel.getCarWithVin(vinNumber)
        }

        viewModel.carProfile.observe(viewLifecycleOwner, Observer { car ->
            if (car != null) {
                vin_txt_input.setText(car.vin)
                engine_model.text = car.engineModel
                engine_cylinders.text = car.engineCylinders.toString()
                power.text = car.engineKW.toString()
                drive.text = car.driveType
                transmission_type.text = car.transmissionStyle ?: "No Data"
                brakes.text = car.brakeSystemType
                displacement.text = car.displacementCC.toString()
                make.text = car.make
                model.text = car.model
                model_year.text = car.modelYear.toString()
                seat.text = car.seats?.let { it.toString() } ?: "No Data"
                doors.text = car.doors?.let { it.toString() } ?: "No Data"
                vehicle_type.text = StringUtils.substringBetween(car.vehicleType, "(", ")")
                drive.text = car.driveType
            }
        })

        vin_txt_input.setOnKeyListener { view, keyCode, keyEvent ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && keyEvent.action == KeyEvent.ACTION_UP) {
                if (vin_txt_input.text.toString().trim().length == 17) {
                    viewModel.getCarWithVin(vin_txt_input.text.toString())
                } else {
                    Toast.makeText(activity, "Invalid VIN number.", Toast.LENGTH_SHORT).show()
                }
                return@setOnKeyListener true
            }
            false
        }

    }

}
