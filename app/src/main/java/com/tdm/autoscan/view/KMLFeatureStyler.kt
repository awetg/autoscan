package com.tdm.autoscan.view

import android.graphics.Color
import android.view.MotionEvent
import org.osmdroid.bonuspack.R
import org.osmdroid.bonuspack.kml.*
import org.osmdroid.views.overlay.Marker
import org.osmdroid.views.overlay.Overlay
import org.osmdroid.views.overlay.Polygon
import org.osmdroid.views.overlay.Polyline
import org.osmdroid.views.overlay.infowindow.BasicInfoWindow

object KMLFeatureStyler : KmlFeature.Styler {

    override fun onPolygon(polygon: Polygon?, kmlPlacemark: KmlPlacemark?, kmlPolygon: KmlPolygon?) {
    }

    override fun onLineString(polyline: Polyline?, kmlPlacemark: KmlPlacemark?, kmlLineString: KmlLineString?) {
        val lineColor = if (kmlPlacemark?.mExtendedData!!["overallRoadCondition"] == "POOR_CONDITION")
            Color.RED
        else
            Color.parseColor("#f9ba13")
        polyline?.color = lineColor
        polyline?.width = 16f
        polyline?.setOnClickListener { polyline1, mapView, eventPos ->
            // this condition won't happen much because the overlay section is small
            if (polyline1.isInfoWindowOpen) {
                polyline1.infoWindow.close()
                polyline1.color = lineColor
            }
            else {
                polyline1.color = Color.BLUE
                val infoWindow = BasicInfoWindow(R.layout.bonuspack_bubble, mapView)
                infoWindow.view.setOnTouchListener { view, motionEvent ->
                    if (motionEvent.action == MotionEvent.ACTION_UP) {
                        polyline1.color = lineColor
                        infoWindow.close()
                    }
                    return@setOnTouchListener true
                }
                polyline1.infoWindow = infoWindow
                polyline1.showInfoWindow()
            }
            true
        }

    }

    override fun onFeature(overlay: Overlay?, kmlFeature: KmlFeature?) {
        kmlFeature?.mVisibility = true
    }

    override fun onPoint(
        marker: Marker?,
        kmlPlacemark: KmlPlacemark?,
        kmlPoint: KmlPoint?
    ) {

    }

    override fun onTrack(polyline: Polyline?, kmlPlacemark: KmlPlacemark?, kmlTrack: KmlTrack?) {

    }
}