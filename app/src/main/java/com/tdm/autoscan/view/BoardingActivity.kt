package com.tdm.autoscan.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.tdm.autoscan.R
import com.tdm.autoscan.adapter.BoardingPagerAdapter
import com.tdm.autoscan.util.BoardingItem
import kotlinx.android.synthetic.main.activity_boarding.*

class BoardingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_boarding)

        val boardingItems = listOf(
            BoardingItem("On Board Diagnostic device", "The app will use vehicles' details collected from OBD devices.", R.drawable.obd_device),
            BoardingItem("Turn off the engine", "For your safety, ensure the vehicle is turned off before you connect the OBD device", R.drawable.engine_start),
            BoardingItem("Port location", "Look for the location to connect the OBD device in your car", R.drawable.obd_location),
            BoardingItem("Connect the device", "After the device light is turned on and off, wait one minute before turning on the vehicles' ignition", R.drawable.status_obd)
            )

        val boardingPagerAdapter =
            BoardingPagerAdapter(this, boardingItems)
        boarding_viewpager.adapter = boardingPagerAdapter
        boarding_tablayout.setupWithViewPager(boarding_viewpager)
        Log.d("DBB", "current itme ${boarding_viewpager.currentItem}")


        get_started_btn.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }

        next_btn.setOnClickListener {
            if(boarding_viewpager.currentItem < boardingItems.size - 1) {
                boarding_viewpager.currentItem++
                Log.d("DBG", "current itme ${boarding_viewpager.currentItem}")
            }
            else {
                // Show get started button
                next_btn.visibility = View.INVISIBLE
                boarding_tablayout.visibility = View.INVISIBLE
                get_started_btn.visibility = View.VISIBLE
            }
        }

    }
}