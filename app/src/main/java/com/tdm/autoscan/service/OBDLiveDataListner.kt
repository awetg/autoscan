package com.tdm.autoscan.service

interface OBDLiveDataListner {
    fun onUpdate(job: OBDCommandJob)
}