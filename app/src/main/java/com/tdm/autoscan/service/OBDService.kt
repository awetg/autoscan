package com.tdm.autoscan.service

import android.app.IntentService
import android.app.Notification
import android.app.PendingIntent
import android.bluetooth.BluetoothSocket
import android.content.Intent
import android.util.Log
import androidx.core.app.NotificationCompat
import com.tdm.autoscan.R
import com.tdm.autoscan.util.BluetoothConnection
import com.tdm.autoscan.util.Constant
import com.tdm.autoscan.util.CustomGaugeItems
import com.tdm.autoscan.view.MainActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.concurrent.ConcurrentHashMap

class OBDService : IntentService("OBDService") {

    companion object {
        private const val EXIT = "EXIT"
        var obdLiveDataListener: OBDLiveDataListner? = null
        val selectedJobs: ConcurrentHashMap<String, OBDCommandJob> = ConcurrentHashMap()
        private var socket: BluetoothSocket? = null
        var useFakeData = false

        @Synchronized
        fun addJob(job: OBDCommandJob) {
            selectedJobs[job.name] = job
        }
    }

    override fun onHandleIntent(p0: Intent?) {}

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        startForeground(1616, createNotification("OBD Live Data"))

        if (intent != null) {
            if (intent.action == EXIT)
                stopSelf()
        }

        if (useFakeData)
            generateFakeData()
        else
            executeCommands()

        return START_STICKY
    }


    private fun createNotification(message: String): Notification {

        // pending intent to open MainActivity when notification is clicked
        val pendingIntent = Intent(this, MainActivity::class.java).let {
            it.action = EXIT
            it.putExtra(Constant.START_FRAGMENT, "LiveDataFragment")
            PendingIntent.getActivity(this, 0 , it, PendingIntent.FLAG_UPDATE_CURRENT)
        }

        val exitPendingIntent = Intent(this, OBDService::class.java).let {
            it.action = EXIT
            PendingIntent.getService(this, 0, it, 0)
        }

        return NotificationCompat.Builder(this, "CHANNEL_ID")
            .setContentText(message)
            .setSmallIcon(R.drawable.ic_launcher_background)
            .setContentIntent(pendingIntent)
            .setTicker(message)
            .addAction(0,"Exit", exitPendingIntent)
            .build()
    }

    private fun executeCommands() {
        GlobalScope.launch(Dispatchers.IO) {
            // if bluetooth is connected configure socket and set it to service socket
            if (BluetoothConnection.socket != null && BluetoothConnection.socket!!.isConnected) {
                BluetoothConnection.configureSocket()
                socket = BluetoothConnection.socket
            }
            // if bluetooth connection is not connected try connecting
            else {
                if (BluetoothConnection.connect(this@OBDService)) {
                    BluetoothConnection.configureSocket()
                    socket = BluetoothConnection.socket
                } else
                    return@launch
            }
            // while there is is a listener and obd device is connected run all selected commands
            while (obdLiveDataListener != null && socket != null) {
                if (selectedJobs.isNotEmpty()) {
                    selectedJobs.values.forEach {
                        try {
                            it.command.run(socket?.inputStream, socket?.outputStream)
                            it.state = OBDCommandJobState.FINISHED
                            obdLiveDataListener?.onUpdate(it)
                        } catch (e: Exception) {
                            Log.d("DBG", "obd command execution error ${e.message}")
                            e.printStackTrace()
                        }
                    }
                }
                delay(500)
            }
        }
    }

    private fun generateFakeData() {
        GlobalScope.launch(Dispatchers.IO) {
            while (obdLiveDataListener != null) {
                if (selectedJobs.isNotEmpty()) {
                    selectedJobs.forEach { (key: String, job: OBDCommandJob) ->
                        val customGauge = CustomGaugeItems.customGauges[key]
                        val mockOBDCommand = OBDCommandMock(customGauge!!.minValue.toInt(), customGauge.maxValue.toInt())
                        obdLiveDataListener?.onUpdate(OBDCommandJob(mockOBDCommand,job.name, OBDCommandJobState.FINISHED))
                    }
                }

                delay(800)
            }
        }
    }
}