package com.tdm.autoscan.service

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.tdm.autoscan.R
import com.tdm.autoscan.network.API
import com.tdm.autoscan.util.Constant
import com.tdm.autoscan.util.FileUtil
import com.tdm.autoscan.view.MainActivity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import okhttp3.ResponseBody

class MyFirebaseMessagingService : FirebaseMessagingService() {

    private val apiResponseCallback = object : API.APIResponseCallback<API.Model.RegionKMLResponse> {
        override fun onSuccess(value: API.Model.RegionKMLResponse) {
            GlobalScope.launch {

                val file = FileUtil.getOrCreateFile(applicationContext, value.regionName, ".zip")
                FileUtil.copyStreamToFile(value.responseBody.byteStream(), file)
                Log.d("DBG", "downloading region file successful for region: ${value.regionName}")
                if (FileUtil.unzipRegionKMLFile(applicationContext, value.regionName)) {
                    val notification = createNotification("Weather Alert")
                    val nm = getSystemService(Service.NOTIFICATION_SERVICE) as NotificationManager
                    nm.notify(1, notification)
                    val intent = Intent(applicationContext, MainActivity::class.java)
                    intent.putExtra(Constant.START_FRAGMENT, "NotificationFragment")
                    startActivity(intent)
                }

            }
        }

        override fun onFailure(throwable: Throwable) {
            Log.d("DBG", "Downloading region kml file failed")
        }
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.d("DBG", "Notification received")
        val data: Map<String, String> = remoteMessage.data
        val type = data["type"]
        if (type == "WEATHER_ALERT") {
            val updateTime = data["dataUpdatedTime"]
            val regionName = data["regionName"]
            Log.d("DBG", "weather alert region $regionName")

            if (regionName != null)
                API.downloadRegionKML(regionName, apiResponseCallback)

            getSharedPreferences(Constant.APP_DATA, Context.MODE_PRIVATE)
                .edit()
                .putString(Constant.ROAD_CONDITION_UPDATE_TIME, updateTime)
                .apply()
        }
        Handler(Looper.getMainLooper()).post {
            Toast.makeText(this, type, Toast.LENGTH_LONG).show()
        }
    }

    override fun onNewToken(token: String) {
        getSharedPreferences("USER_DATA", Context.MODE_PRIVATE)
            .edit()
            .putString("TOKEN", token)
            .apply()
    }

    private fun createNotification(message: String): Notification {

        // pending intent to ope MainActivity when notification is clicked
        val pendingIntent = Intent(this, MainActivity::class.java).let {
            PendingIntent.getActivity(this, 0 , it, 0)
        }

        return NotificationCompat.Builder(this, "CHANNEL_ID")
            .setContentText(message)
            .setSmallIcon(R.drawable.ic_launcher_background)
            .setContentIntent(pendingIntent)
            .setTicker(message)
            .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .build()
    }
}