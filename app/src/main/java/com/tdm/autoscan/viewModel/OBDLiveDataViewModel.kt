package com.tdm.autoscan.viewModel

import android.app.Application
import android.content.Intent
import androidx.core.content.ContextCompat
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.tdm.autoscan.service.OBDCommandJob
import com.tdm.autoscan.service.OBDLiveDataListner
import com.tdm.autoscan.service.OBDService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class OBDLiveDataViewModel(application: Application): AndroidViewModel(application), OBDLiveDataListner {

    private val _obdCommandJob = MutableLiveData<OBDCommandJob?>().apply { value = null }

    val obdCommandJob: LiveData<OBDCommandJob?> = _obdCommandJob

    override fun onUpdate(job: OBDCommandJob) {
        GlobalScope.launch(Dispatchers.Main) {
            _obdCommandJob.value = job
        }
    }

    fun startService() {
        ContextCompat.startForegroundService(getApplication(), Intent(getApplication(), OBDService::class.java))
        OBDService.obdLiveDataListener = this
    }

    fun addJob(job: OBDCommandJob) = OBDService.addJob(job)

    fun removeJob(name: String) = OBDService.selectedJobs.remove(name)

    override fun onCleared() {
        OBDService.obdLiveDataListener = null
        super.onCleared()
    }
}