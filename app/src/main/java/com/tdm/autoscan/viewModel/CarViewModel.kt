package com.tdm.autoscan.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.tdm.autoscan.model.AppRepository
import com.tdm.autoscan.model.room.Car
import com.tdm.autoscan.model.room.MyDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class CarViewModel (application: Application):AndroidViewModel(application){

    val carProfile: LiveData<Car?> = AppRepository.carProfile
    fun getCarWithVin(vin: String) = AppRepository.getCarProfile(vin, getApplication())
}