[![pipeline status](https://gitlab.com/awetg/autoscan/badges/master/pipeline.svg)](https://gitlab.com/awetg/autoscan/commits/dev)


# AutoScan

## Overview
AutoScan is an Android app that uses bluetooth to connect to your OBD2 device and show diagonstic information about your car.

## App features
* Show diagonstic car information


## Build 
Clone this repository and import into **Android Studio**
```bash
git clone git@gitlab.com:awetg/autoscan.git 
```
Build it usign Android Studio

## API Reference
* Minimum SDK: 24
* Target SDK: 29   

## Screenshot 

## License


## Contributors
### Gitlab users
* [awetg](https://gitlab.com/awetg)
* [bishwasshrestha](https://gitlab.com/bishwas.lab)
* [Ishup Khan](https://gitlab.com/khan.alishup)
* [Utsab Kharel](https://gitlab.com/utsabk)